# eventsourced Readview handler

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/eventsourced/readview)](https://goreportcard.com/report/gitlab.com/unboundsoftware/eventsourced/readview) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/readview?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/readview) [![Build Status](https://gitlab.com/unboundsoftware/eventsourced/readview/badges/main/pipeline.svg)](https://gitlab.com/unboundsoftware/eventsourced/readview/commits/main)[![coverage report](https://gitlab.com/unboundsoftware/eventsourced/readview/badges/main/coverage.svg)](https://gitlab.com/unboundsoftware/eventsourced/readview/commits/main)

Package `readview` provides ... which can be used with the [eventsourced framework](https://gitlab.com/unboundsoftware/eventsourced/eventsourced).

Download:
```shell
go get gitlab.com/unboundsoftware/eventsourced/readview
```



