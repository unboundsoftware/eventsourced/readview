/*
 * MIT License
 *
 * Copyright (c) 2025 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package readview

import (
	"errors"
	"fmt"
	"reflect"

	"github.com/sparetimecoders/goamqp"
	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

type Connection interface {
	TypeMappingHandler(handler goamqp.HandlerFunc) goamqp.HandlerFunc
}

func (r *Readview) RegularMappingHandler(conn Connection, name string, messageHandler MessageHandler) goamqp.HandlerFunc {
	return conn.TypeMappingHandler(func(msg any, headers goamqp.Headers) (response any, err error) {
		if _, ok := msg.(eventsourced.Event); !ok {
			r.logger.Warn(fmt.Sprintf("Got non event for readview '%s', %s", name, reflect.TypeOf(msg).String()))
			return nil, nil
		}
		if _, ok := msg.(eventsourced.ExternalSource); ok {
			return nil, nil
		}
		response, err = messageHandler(msg)
		if err != nil && errors.Is(err, ErrStatusRowLocked) {
			return response, fmt.Errorf("%w: status row locked", goamqp.ErrRecoverable)
		}
		return response, err
	})
}

func (r *Readview) ExternalMappingHandler(conn Connection, messageHandler MessageHandler) goamqp.HandlerFunc {
	return conn.TypeMappingHandler(func(msg any, headers goamqp.Headers) (response any, err error) {
		response, err = messageHandler(msg)
		if err != nil && errors.Is(err, ErrStatusRowLocked) {
			return response, fmt.Errorf("%w: status row locked", goamqp.ErrRecoverable)
		}
		return response, err
	})
}
