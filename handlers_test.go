/*
 * MIT License
 *
 * Copyright (c) 2025 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package readview

import (
	"errors"
	"fmt"
	"log/slog"
	"strings"
	"testing"

	"github.com/sparetimecoders/goamqp"
	"github.com/stretchr/testify/assert"
)

func TestReadview_RegularMappingHandler(t *testing.T) {
	type args struct {
		name           string
		messageHandler MessageHandler
		event          any
	}
	tests := []struct {
		name       string
		args       args
		want       any
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "non-event type is ignored",
			args: args{
				name:  "some-readview",
				event: "error",
			},
			want:       nil,
			wantErr:    assert.NoError,
			wantLogged: []string{"level=WARN msg=\"Got non event for readview 'some-readview', string\""},
		},
		{
			name: "external event type is ignored",
			args: args{
				name:  "some-readview",
				event: &ExternalEvent{},
			},
			want:       nil,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name: "status row locked is handled as a recoverable error",
			args: args{
				name:  "some-readview",
				event: &TestEvent{},
				messageHandler: func(msg any) (any, error) {
					return nil, ErrStatusRowLocked
				},
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.ErrorIs(t, err, goamqp.ErrRecoverable)
			},
			wantLogged: nil,
		},
		{
			name: "response and error is returned",
			args: args{
				name:  "some-readview",
				event: &TestEvent{},
				messageHandler: func(msg any) (any, error) {
					return "response", errors.New("some error")
				},
			},
			want: "response",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "some error")
			},
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &Buffer{}
			logger := slog.New(slog.NewTextHandler(logged, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			r := &Readview{
				logger: logger,
			}
			conn := &MockConnection{}
			handler := r.RegularMappingHandler(conn, tt.args.name, tt.args.messageHandler)
			response, err := handler(tt.args.event, nil)
			if !tt.wantErr(t, err, fmt.Sprintf("ReadView_RegularMappingHandler(%v, %v)", conn, tt.args.name)) {
				return
			}
			assert.Equal(t, tt.want, response, fmt.Sprintf("ReadView_RegularMappingHandler(%v, %v)", conn, tt.args.name))
			var gotLogged []string
			if logged.String() != "" {
				gotLogged = strings.Split(logged.String(), "\n")
				gotLogged = gotLogged[:len(gotLogged)-1]
			}

			assert.Equal(t, tt.wantLogged, gotLogged)
		})
	}
}

func TestReadview_ExternalMappingHandler(t *testing.T) {
	type args struct {
		messageHandler MessageHandler
		event          any
	}
	tests := []struct {
		name    string
		args    args
		want    any
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "status row locked is handled as a recoverable error",
			args: args{
				event: &TestEvent{},
				messageHandler: func(msg any) (any, error) {
					return nil, ErrStatusRowLocked
				},
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.ErrorIs(t, err, goamqp.ErrRecoverable)
			},
		},
		{
			name: "response and error is returned",
			args: args{
				event: &TestEvent{},
				messageHandler: func(msg any) (any, error) {
					return "response", errors.New("some error")
				},
			},
			want: "response",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "some error")
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Readview{}
			conn := &MockConnection{}
			handler := r.ExternalMappingHandler(conn, tt.args.messageHandler)
			response, err := handler(tt.args.event, nil)
			if !tt.wantErr(t, err, fmt.Sprintf("ReadView_ExternalMappingHandler(%v)", conn)) {
				return
			}
			assert.Equal(t, tt.want, response, fmt.Sprintf("ReadView_ExternalMappingHandler(%v)", conn))
		})
	}
}

type MockConnection struct {
}

func (m MockConnection) TypeMappingHandler(handler goamqp.HandlerFunc) goamqp.HandlerFunc {
	return handler
}

var _ Connection = &MockConnection{}
