-- +goose Up
alter table readview_status
    add column filler text,
    add column started timestamptz,
    drop column back_fill;

-- +goose Down
alter table readview_status
    drop column filler,
    drop column started,
    add column back_fill boolean default false;
