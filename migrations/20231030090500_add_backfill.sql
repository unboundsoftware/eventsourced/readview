-- +goose Up
alter table readview_status
    add column back_fill boolean default false;

-- +goose Down
alter table readview_status
    drop column back_fill;
