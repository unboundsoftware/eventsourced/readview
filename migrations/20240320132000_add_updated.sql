-- +goose Up
alter table readview_status
    add column updated timestamptz;

-- +goose Down
alter table readview_status
    drop column updated;
