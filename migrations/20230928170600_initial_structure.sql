-- +goose Up
create table if not exists readview_status
(
    name           text not null UNIQUE,
    current_seq_no int  not null
);

-- +goose Down
drop table readview_status;
