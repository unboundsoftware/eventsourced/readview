/*
 * MIT License
 *
 * Copyright (c) 2024 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package readview

import (
	"fmt"
	"sync/atomic"
	"time"
)

func Register(name string, handler EventHandler, resetHandler ResetHandler) Opt {
	return func(r *Readview) error {
		if _, exists := r.viewInfo[name]; exists {
			return fmt.Errorf("readview named '%s' has already been registered", name)
		}
		r.viewInfo[name] = ViewInfo{
			eventHandler:   handler,
			resetHandler:   resetHandler,
			messageHandler: nil,
			readyForEvents: &atomic.Bool{},
		}
		return nil
	}
}

func WithLogger(logger Logger) Opt {
	return func(r *Readview) error {
		r.logger = logger
		return nil
	}
}

func WithBackFillBatchSize(size int) Opt {
	return func(r *Readview) error {
		r.backFillBatchSize = size
		return nil
	}
}

func WithHeartbeat(heartbeat time.Duration) Opt {
	return func(r *Readview) error {
		r.heartbeat = heartbeat
		return nil
	}
}
