module gitlab.com/unboundsoftware/eventsourced/readview

go 1.23.0

toolchain go1.24.1

require (
	github.com/DATA-DOG/go-sqlmock v1.5.2
	github.com/jmoiron/sqlx v1.4.0
	github.com/lib/pq v1.10.9
	github.com/pressly/goose/v3 v3.24.1
	github.com/sparetimecoders/goamqp v0.3.1
	github.com/stretchr/testify v1.10.0
	gitlab.com/unboundsoftware/eventsourced/eventsourced v1.18.1
	gitlab.com/unboundsoftware/eventsourced/mocks v0.6.4
	golang.org/x/sync v0.12.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/mfridman/interpolate v0.0.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rabbitmq/amqp091-go v1.10.0 // indirect
	github.com/sanity-io/litter v1.5.5 // indirect
	github.com/sethvargo/go-retry v0.3.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
