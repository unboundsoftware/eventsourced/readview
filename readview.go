/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package readview

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"reflect"
	"sync/atomic"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/pressly/goose/v3"
	"github.com/pressly/goose/v3/database"
	"golang.org/x/sync/errgroup"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"

	"gitlab.com/unboundsoftware/eventsourced/readview/migrations"
)

type EventHandler func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error

type MessageHandler func(msg any) (any, error)

type ResetHandler func(ctx context.Context, name string, tx *sqlx.Tx) error

var ErrStatusRowLocked = errors.New("status row locked")

type ViewInfo struct {
	eventHandler   EventHandler
	messageHandler MessageHandler
	resetHandler   ResetHandler
	readyForEvents *atomic.Bool
}

type Readview struct {
	db                *sqlx.DB
	store             eventsourced.EventStore
	viewInfo          map[string]ViewInfo
	logger            Logger
	backFillBatchSize int
	hostname          string
	heartbeat         time.Duration
	now               func() time.Time
	sleep             func(d time.Duration)
}

type Logger interface {
	Info(msg string, args ...any)
	Warn(msg string, args ...any)
	Error(msg string, args ...any)
}

type Opt func(r *Readview) error

var hostname = os.Hostname

func New(db *sqlx.DB, store eventsourced.EventStore, opts ...Opt) (*Readview, error) {
	host, err := hostname()
	if err != nil {
		return nil, err
	}
	r := &Readview{
		db:                db,
		store:             store,
		viewInfo:          make(map[string]ViewInfo),
		logger:            slog.Default(),
		backFillBatchSize: 10,
		hostname:          host,
		heartbeat:         time.Second * 5,
		now:               time.Now,
		sleep:             time.Sleep,
	}
	for _, opt := range opts {
		if err := opt(r); err != nil {
			return nil, err
		}
	}
	if err := runMigrations(context.Background(), db.DB, r.logger); err != nil {
		return nil, err
	}
	r.init()
	return r, nil
}

func (r *Readview) Handlers() map[string]MessageHandler {
	handlers := make(map[string]MessageHandler)
	for k, v := range r.viewInfo {
		handlers[k] = v.messageHandler
	}
	return handlers
}

func (r *Readview) Start(ctx context.Context) error {
	maxSeqNo, err := r.store.MaxGlobalSequenceNo(ctx)
	if err != nil {
		return err
	}
	eg := errgroup.Group{}
	for k, v := range r.viewInfo {
		loggedWait := false
		name := k
		info := v
		if _, err := r.db.ExecContext(ctx, "INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING", name); err != nil {
			return err
		}
		eg.Go(func() error {
		start:
			err := startFn(r, name, info, maxSeqNo)
			if errors.Is(err, ErrStatusRowLocked) {
				if !loggedWait {
					loggedWait = true
					r.logger.Info("waiting for back fill", "readview", name)
				}
				time.Sleep(5 * time.Second)
				goto start
			}
			return err
		})
	}
	return eg.Wait()
}

func startFn(r *Readview, name string, info ViewInfo, maxSeqNo int) error {
	resetHandler := info.resetHandler

	ctx := context.Background()
	tx, err := r.db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if err := tx.Rollback(); err != nil && !errors.Is(err, sql.ErrTxDone) {
			r.logger.Error("transaction rollback", "error", err, "readview", name)
		}
	}()
	var currentSeqNo int
	var filler *string
	var updated *time.Time
	row := tx.QueryRowxContext(ctx, "SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT", name)
	err = row.Scan(&currentSeqNo, &filler, &updated)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		var pqErr *pq.Error
		if ok := errors.As(err, &pqErr); ok && pqErr.SQLState() == "55P03" {
			return ErrStatusRowLocked
		}
		return err
	}
	if r.backFillInProgress(filler, updated) {
		return ErrStatusRowLocked
	}

	if currentSeqNo == 0 {
		err := resetHandler(ctx, name, tx)
		if err != nil {
			return fmt.Errorf("reset readview '%s': %w", name, err)
		}
	}
	diff := maxSeqNo - currentSeqNo
	if diff > 0 {
		if _, err := tx.ExecContext(ctx, "INSERT INTO readview_status (name, current_seq_no, updated, filler) VALUES ($1, 0, $3, $2) ON CONFLICT (name) DO UPDATE SET filler = $2, started = $3, updated = $3 WHERE readview_status.name = $1", name, r.hostname, r.now()); err != nil {
			return err
		}
		if err := tx.Commit(); err != nil && !errors.Is(err, sql.ErrTxDone) {
			return err
		}
		// TODO: context.WithCancel? WithTimout even?
		if err := r.initiateBackFill(ctx, name, info, diff, maxSeqNo, currentSeqNo); err != nil {
			return err
		}
	} else {
		info.readyForEvents.Store(true)
	}
	return nil
}

func (r *Readview) Reset(ctx context.Context, name string) error {
	info, err := r.getViewInfo(name)
	if err != nil {
		r.logger.Error(err.Error())
		return nil
	}
	info.readyForEvents.Store(false)
	tx, err := r.db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if err := tx.Rollback(); err != nil && !errors.Is(err, sql.ErrTxDone) {
			r.logger.Error("transaction rollback", "error", err, "readview", name)
		}
	}()
	var currentSeqNo int
	var filler *string
	var updated *time.Time
	row := tx.QueryRowxContext(ctx, "SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT", name)
	err = row.Scan(&currentSeqNo, &filler, &updated)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		var pqErr *pq.Error
		if ok := errors.As(err, &pqErr); ok && pqErr.SQLState() == "55P03" {
			return ErrStatusRowLocked
		}
		return err
	}
	if r.backFillInProgress(filler, updated) {
		r.logger.Warn("backfill already in progress", "readview", name)
		return nil
	}

	if err := info.resetHandler(ctx, name, tx); err != nil {
		return err
	}
	if _, err := tx.ExecContext(ctx, "DELETE FROM readview_status WHERE name = $1", name); err != nil {
		return err
	}
	if err := tx.Commit(); err != nil {
		return err
	}
	maxSeqNo, err := r.store.MaxGlobalSequenceNo(ctx)
	if err != nil {
		return err
	}
	if err := r.initiateBackFill(ctx, name, info, maxSeqNo, maxSeqNo, 0); err != nil {
		return err
	}
	info.readyForEvents.Store(true)
	return nil
}

var ErrNotReadyToProcessEvents = fmt.Errorf("not ready to process events")

func (r *Readview) init() {
	for k, v := range r.viewInfo {
		name := k
		info := v
		readyForEvents := v.readyForEvents
		backOff := 50 * time.Millisecond
		info.messageHandler = func(msg any) (any, error) {
			if !readyForEvents.Load() {
				r.sleep(backOff)
				if backOff < time.Second {
					backOff = backOff * 2
				}
				return nil, fmt.Errorf("readview '%s', %w", name, ErrNotReadyToProcessEvents)
			}
			backOff = 50 * time.Millisecond
			ev, ok := msg.(eventsourced.Event)
			if !ok {
				return nil, fmt.Errorf("readview '%s' got non eventsourced.Event message (%s): %v", name, reflect.TypeOf(msg).String(), msg)
			}
			ctx := context.Background()
			tx, err := r.db.Beginx()
			if err != nil {
				return nil, err
			}
			defer func() {
				if err := tx.Rollback(); err != nil && !errors.Is(err, sql.ErrTxDone) {
					r.logger.Error("transaction rollback", "error", err, "readview", name)
				}
			}()
			var currentSeqNo int
			var filler *string
			var updated *time.Time
			row := tx.QueryRowxContext(ctx, "SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT", name)
			err = row.Scan(&currentSeqNo, &filler, &updated)
			if err != nil {
				var pqErr *pq.Error
				if ok := errors.As(err, &pqErr); ok && pqErr.SQLState() == "55P03" || errors.Is(err, sql.ErrNoRows) {
					if err := tx.Rollback(); err != nil && !errors.Is(err, sql.ErrTxDone) {
						r.logger.Error("transaction rollback", "error", err, "readview", name)
					}
					time.Sleep(time.Second)
					return nil, ErrStatusRowLocked
				}
				return nil, err
			}
			diff := ev.GlobalSequenceNo() - currentSeqNo
			if r.backFillInProgress(filler, updated) {
				if err := tx.Rollback(); err != nil && !errors.Is(err, sql.ErrTxDone) {
					r.logger.Error("transaction rollback", "error", err, "readview", name)
				}
				if diff > 20000 {
					r.sleep(20 * time.Second)
				} else {
					r.sleep(time.Duration(diff) * time.Millisecond)
				}
				return nil, fmt.Errorf("readview '%s' back fill in progress, remaining: %d", name, diff)
			}
			if diff <= 0 {
				// Message already processed
				return nil, nil
			} else if diff == 1 {
				err := r.handle(ctx, ev, name, info.eventHandler, tx)
				if err != nil {
					return nil, fmt.Errorf("readview '%s' process: %w", name, err)
				}
				return nil, nil
			}

			if _, err = tx.ExecContext(ctx, "UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1", name, r.hostname, r.now()); err != nil {
				return nil, err
			}
			if err := tx.Commit(); err != nil && !errors.Is(err, sql.ErrTxDone) {
				return nil, err
			}
			if err := r.initiateBackFill(ctx, name, info, diff, ev.GlobalSequenceNo(), currentSeqNo); err != nil {
				if _, err2 := r.db.ExecContext(ctx, "UPDATE readview_status SET filler = '', started = NULL WHERE name = $1", name); err2 != nil {
					return nil, err2
				}
				return nil, err
			}
			return nil, fmt.Errorf("readview '%s' back fill in progress, remaining: %d", name, diff)
		}
		r.viewInfo[name] = info
	}
}

func (r *Readview) backFillInProgress(filler *string, updated *time.Time) bool {
	return filler != nil && *filler != "" && (*filler == r.hostname || (updated != nil && !updated.Add(r.heartbeat).Before(r.now())))
}

func safeClose(ch chan eventsourced.Event) {
	defer func() {
		_ = recover()
	}()

	close(ch) // panic if ch is closed
}

func (r *Readview) initiateBackFill(ctx context.Context, name string, status ViewInfo, diff int, maxSeqNo int, currentSeqNo int) error {
	status.readyForEvents.Store(false)
	handler := status.eventHandler
	ch := make(chan eventsourced.Event, 10)
	defer safeClose(ch)
	eg, ctx := errgroup.WithContext(ctx)
	r.logger.Info(fmt.Sprintf("starting back fill, events to process: %d", diff), "readview", name)
	eg.Go(func() error {
		err := r.doBackFill(ctx, name, handler, ch, maxSeqNo, currentSeqNo)
		if err != nil {
			return fmt.Errorf("readview '%s' back fill failed: %w", name, err)
		}
		return nil
	})
	eg.Go(func() error {
		return r.store.Events(ctx, currentSeqNo, ch)
	})
	err := eg.Wait()
	status.readyForEvents.Store(true)
	return err
}

func (r *Readview) doBackFill(ctx context.Context, name string, handler EventHandler, ch chan eventsourced.Event, maxSequenceNo int, currentSeqNo int) error {
	totalToProcess := maxSequenceNo - currentSeqNo
	tx, err := r.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  false,
	})
	if err != nil {
		if _, err2 := r.db.ExecContext(ctx, "UPDATE readview_status SET filler = '', started = NULL WHERE name = $1", name); err2 != nil {
			return err2
		}
		r.logger.Error("create tx", "error", err, "readview", name)
		return err
	}
	defer func() {
		if tx != nil {
			err := tx.Rollback()
			if err != nil && !errors.Is(err, sql.ErrTxDone) {
				r.logger.Error("rollback tx", "error", err, "readview", name)
			}
		}
		if _, err = r.db.ExecContext(ctx, "UPDATE readview_status SET filler = '', started = NULL WHERE name = $1", name); err != nil {
			r.logger.Error("reset back fill status", "error", err, "readview", name)
		}
	}()

	start := r.now()
	done := false
	for !done {
		select {
		case <-ctx.Done():
			r.logger.Info("root context done")
			done = true
			break
		case evt := <-ch:
			if evt == nil {
				r.logger.Info("events processed", "readview", name)
				done = true
				ctx = context.Background()
				_, err = tx.ExecContext(ctx,
					"INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1",
					name, currentSeqNo, r.now())
				if err != nil {
					return r.rollback(tx, err, name, "update current seq_no, rolling back")
				}
				err = tx.Commit()
				if err != nil && !errors.Is(err, sql.ErrTxDone) {
					r.logger.Error("commit tx", "error", err, "readview", name)
					return err
				}
				r.logger.Info("back fill complete", "readview", name)
				break
			}

			currentSeqNo = evt.GlobalSequenceNo()
			if external, ok := evt.(interface{ Unwrap() eventsourced.Event }); ok {
				evt = external.Unwrap()
			}
			if err := handler(ctx, tx, evt); err != nil {
				return r.rollback(tx, err, name, "process back fill event %d", evt.GlobalSequenceNo())
			}

			if currentSeqNo%r.backFillBatchSize == 0 {
				_, err = tx.ExecContext(ctx,
					"INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1",
					name, currentSeqNo, r.now())
				if err != nil {
					return r.rollback(tx, err, name, "update current seq_no, rolling back")
				}
				err = tx.Commit()
				if err != nil {
					r.logger.Error("commit tx", "error", err, "readview", name)
					return err
				}
				tx, err = r.db.BeginTx(ctx, &sql.TxOptions{
					Isolation: 0,
					ReadOnly:  false,
				})
				if err != nil {
					r.logger.Error("create tx", "error", err, "readview", name)
					return err
				}
				timeForProcessed := r.now().Sub(start)
				remaining := maxSequenceNo - currentSeqNo
				timeRemaining := calcRemainingTime(remaining, totalToProcess, timeForProcessed)
				r.logger.Info(fmt.Sprintf("remaining back fill events: %d/%d, estimated time to completion: %s", remaining, totalToProcess, timeRemaining), "readview", name)
			}
		}
	}

	return nil
}

func calcRemainingTime(remaining, total int, processTime time.Duration) time.Duration {
	return time.Duration(float64(processTime) / float64(total-remaining) * float64(remaining))
}

func (r *Readview) handle(ctx context.Context, event eventsourced.Event, name string, handler EventHandler, tx *sqlx.Tx) error {
	evt := event
	if ec, ok := event.(*eventsourced.ExternalEvent[eventsourced.Event]); ok {
		evt = ec.WrappedEvent
	}
	if err := handler(ctx, tx.Tx, evt); err != nil {
		return r.rollback(tx.Tx, err, name, "process event %d", event.GlobalSequenceNo())
	}
	if _, external := event.(eventsourced.ExternalSource); !external {
		_, err := tx.ExecContext(ctx,
			"INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1",
			name, event.GlobalSequenceNo(), r.now())
		if err != nil {
			return r.rollback(tx.Tx, err, name, "update current seq_no, rolling back")
		}
	}
	err := tx.Commit()
	if err != nil {
		r.logger.Error("commit tx", "error", err, "readview", name)
		return err
	}
	return nil
}

func (r *Readview) rollback(tx *sql.Tx, err error, name, msg string, args ...any) error {
	r.logger.Error(fmt.Sprintf(msg, args...), "error", err, "readview", name)
	rErr := tx.Rollback()
	if rErr != nil && !errors.Is(err, sql.ErrTxDone) {
		r.logger.Error("rollback tx", "error", rErr, "readview", name)
	}
	return err
}

func (r *Readview) getViewInfo(name string) (ViewInfo, error) {
	info, ok := r.viewInfo[name]
	if !ok {
		return ViewInfo{}, fmt.Errorf("no readview named '%s' exist", name)
	}
	return info, nil
}

func runMigrations(ctx context.Context, db *sql.DB, logger Logger) error {
	gooseLogger := gooseLoggerWrapper{logger: logger}
	goose.SetLogger(gooseLogger)
	store, err := database.NewStore(database.DialectPostgres, "goose_db_version_readview")
	if err != nil {
		return err
	}
	provider, err := goose.NewProvider("", db, migrations.FS, goose.WithStore(store))
	if err != nil {
		return err
	}
	_, err = provider.Up(ctx)
	return err
}

type gooseLoggerWrapper struct {
	logger Logger
}

func (g gooseLoggerWrapper) Fatalf(format string, v ...interface{}) {
	g.logger.Error(fmt.Sprintf(format, v...), "part", "readview")
}

func (g gooseLoggerWrapper) Printf(format string, v ...interface{}) {
	g.logger.Info(fmt.Sprintf(format, v...), "part", "readview")
}
