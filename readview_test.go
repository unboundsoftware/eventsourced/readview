/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package readview

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"os"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/unboundsoftware/eventsourced/mocks"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

func TestNew(t *testing.T) {
	var noOpResetHandler ResetHandler = func(ctx context.Context, name string, tx *sqlx.Tx) error {
		return nil
	}
	type args struct {
		opts []Opt
	}
	tests := []struct {
		name      string
		args      args
		hostname  func() (string, error)
		noDb      bool
		setup     func(mock sqlmock.Sqlmock)
		wantViews []string
		wantErr   assert.ErrorAssertionFunc
	}{
		{
			name:      "no db provided",
			args:      args{},
			noDb:      true,
			wantViews: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "db must not be nil")
			},
		},
		{
			name: "migration error",
			args: args{},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT EXISTS ( SELECT 1 FROM pg_tables WHERE (current_schema() IS NULL OR schemaname = current_schema()) AND tablename = 'goose_db_version_readview' )").WillReturnError(fmt.Errorf("error"))
			},
			wantViews: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "failed to initialize: failed to check if version table exists: failed to check if table exists: error")
			},
		},
		{
			name: "hostname error",
			args: args{},
			hostname: func() (string, error) {
				return "", fmt.Errorf("hostname error")
			},
			wantViews: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "hostname error")
			},
		},
		{
			name: "no opts",
			args: args{},
			setup: func(mock sqlmock.Sqlmock) {
				MigrationExpectations(mock)
			},
			wantViews: nil,
			wantErr:   assert.NoError,
		},
		{
			name: "opts error",
			args: args{
				opts: []Opt{
					Register("test", func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
						return nil
					}, noOpResetHandler),
					Register("test", func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
						return nil
					}, noOpResetHandler),
				},
			},
			wantViews: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview named 'test' has already been registered")
			},
		},
		{
			name: "init success",
			args: args{
				opts: []Opt{
					Register("test", func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
						return nil
					}, noOpResetHandler),
				},
			},
			setup: func(mock sqlmock.Sqlmock) {
				MigrationExpectations(mock)
			},
			wantViews: []string{"test"},
			wantErr:   assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sqlDb, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			assert.NoError(t, err)
			var store eventsourced.EventStore
			if tt.hostname == nil {
				hostname = os.Hostname
			} else {
				hostname = tt.hostname
			}
			if tt.setup != nil {
				tt.setup(mock)
			}
			db := sqlx.NewDb(sqlDb, "sqlmock")
			if tt.noDb {
				db = &sqlx.DB{}
			}
			got, err := New(db, store, tt.args.opts...)
			if !tt.wantErr(t, err, fmt.Sprintf("New(%v, %v, %v)", db, store, tt.args.opts)) {
				return
			}
			if got != nil {
				got.db = nil
			}
			var views []string
			if got != nil {
				for k := range got.viewInfo {
					views = append(views, k)
				}
			}
			assert.Equalf(t, tt.wantViews, views, "New(%v, %v, %v)", db, store, tt.args.opts)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestReadview_MessageHandler(t *testing.T) {
	now := time.Date(2023, 11, 3, 16, 12, 0, 0, time.Local)
	tests := []struct {
		name               string
		store              func(t *testing.T) eventsourced.EventStore
		handler            func(t *testing.T) func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error
		resetHandler       func(t *testing.T) ResetHandler
		setup              func(mock sqlmock.Sqlmock)
		messages           []any
		wantProcessed      int
		wantErr            assert.ErrorAssertionFunc
		wantLogged         []string
		processingDisabled bool
	}{
		{
			name: "event processing disabled returns error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			messages:      []any{"abc"},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test', not ready to process events")
			},
			processingDisabled: true,
		},
		{
			name: "non event message error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			messages:      []any{"abc"},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' got non eventsourced.Event message (string): abc")
			},
		},
		{
			name: "begin transaction error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin().WillReturnError(fmt.Errorf("transaction error"))
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "transaction error")
			},
			wantLogged: nil,
		},
		{
			name: "query status error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnError(fmt.Errorf("query error"))
				mock.ExpectRollback()
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "query error")
			},
			wantLogged: nil,
		},
		{
			name: "lock error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				lockError := &pq.Error{Code: "55P03", Message: "lock_not_available"}
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnError(lockError)
				mock.ExpectRollback()
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "status row locked")
			},
			wantLogged: nil,
		},
		{
			name: "lock+rollback error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				lockError := &pq.Error{Code: "55P03", Message: "lock_not_available"}
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnError(lockError)
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "status row locked")
			},
			wantLogged: []string{"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test"},
		},
		{
			name: "back fill in progress - rollback error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}).AddRow("0", "host", time.Time{}))
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' back fill in progress, remaining: 0")
			},
			wantLogged: []string{
				"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test",
			},
		},
		{
			name: "back fill in progress - large diff",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}).AddRow("0", "host", time.Time{}))
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			messages:      []any{&TestEvent{seqNo: 20001}},
			wantProcessed: 0,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' back fill in progress, remaining: 20001")
			},
			wantLogged: []string{"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test"},
		},
		{
			name: "stale backfill",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(1, "other-host", time.Time{}),
					)
				mock.ExpectRollback()
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr:       assert.NoError,
		},
		{
			name: "event already handled",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(1, "", nil),
					)
				mock.ExpectRollback()
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr:       assert.NoError,
		},
		{
			name: "event already handled - rollback error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(1, "", nil),
					)
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			messages:      []any{&TestEvent{}},
			wantProcessed: 0,
			wantErr:       assert.NoError,
			wantLogged:    []string{"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test"},
		},
		{
			name: "event is current - handler error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			handler: func(t *testing.T) func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
				return func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					assert.Equal(t, 1, event.GlobalSequenceNo())
					return fmt.Errorf("handler error")
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.ExpectRollback()
			},
			messages:      []any{&TestEvent{seqNo: 1}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' process: handler error")
			},
			wantLogged: []string{"level=ERROR msg=\"process event 1\" error=\"handler error\" readview=test"},
		},
		{
			name: "event is current - update status error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, now).
					WillReturnError(fmt.Errorf("update status error"))
				mock.ExpectRollback()
			},
			messages:      []any{&TestEvent{seqNo: 1}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' process: update status error")
			},
			wantLogged: []string{"level=ERROR msg=\"update current seq_no, rolling back\" error=\"update status error\" readview=test"},
		},
		{
			name: "event is current - commit error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit().WillReturnError(fmt.Errorf("commit error"))
			},
			messages:      []any{&TestEvent{seqNo: 1}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' process: commit error")
			},
			wantLogged: []string{"level=ERROR msg=\"commit tx\" error=\"commit error\" readview=test"},
		},
		{
			name: "event is current - success",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
			},
			messages:      []any{&TestEvent{seqNo: 1}},
			wantProcessed: 1,
			wantErr:       assert.NoError,
		},
		{
			name: "event in external container is current - success",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
			},
			handler: func(t *testing.T) func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
				return func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					assert.IsType(t, &ExternalEvent{}, event)
					return nil
				}
			},
			messages: []any{&eventsourced.ExternalEvent[eventsourced.Event]{
				BaseEvent: eventsourced.BaseEvent{
					EventGlobalSequence: eventsourced.EventGlobalSequence{SeqNo: 1},
				},
				WrappedEvent: &ExternalEvent{},
			}},
			wantProcessed: 0,
			wantErr:       assert.NoError,
		},
		{
			name: "backfill needed - update status error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnError(fmt.Errorf("update status error"))
				mock.ExpectRollback()
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
		},
		{
			name: "backfill needed - update status error - rollback error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnError(fmt.Errorf("update status error"))
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
			wantLogged: []string{"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test"},
		},
		{
			name: "backfill needed - update status - commit error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit().WillReturnError(fmt.Errorf("commit error"))
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "commit error")
			},
		},
		{
			name: "backfill needed - begin TX error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						close(c)
						time.Sleep(10 * time.Millisecond)
						return nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectBegin().
					WillReturnError(fmt.Errorf("tx error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' back fill failed: tx error")
			},
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 2\" readview=test",
				"level=ERROR msg=\"create tx\" error=\"tx error\" readview=test",
			},
		},
		{
			name: "backfill needed - begin TX+update status error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						close(c)
						time.Sleep(10 * time.Millisecond)
						return nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectBegin().
					WillReturnError(fmt.Errorf("tx error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnError(fmt.Errorf("update status error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnError(fmt.Errorf("update status error"))
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 2\" readview=test",
			},
		},
		{
			name: "backfill needed - events error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						assert.Equal(t, 0, fromID)
						assert.Nil(t, aggregateTypes)
						close(c)
						time.Sleep(100 * time.Millisecond)
						return fmt.Errorf("events error")
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 0, now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "events error")
			},
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 2\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
		{
			name: "backfill needed - events+update status error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						assert.Equal(t, 0, fromID)
						assert.Nil(t, aggregateTypes)
						close(c)
						time.Sleep(100 * time.Millisecond)
						return fmt.Errorf("events error")
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnError(fmt.Errorf("update status error"))
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 0, now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "events error")
			},
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 2\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
				"level=ERROR msg=\"reset back fill status\" error=\"update status error\" readview=test",
			},
		},
		{
			name: "backfill needed - success",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						assert.Equal(t, 0, fromID)
						assert.Nil(t, aggregateTypes)
						close(c)
						time.Sleep(100 * time.Millisecond)
						return nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.
					ExpectExec("UPDATE readview_status SET filler = $2, started = $3, updated = $3 WHERE name = $1").
					WithArgs("test", "host", now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 0, now).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages:      []any{&TestEvent{seqNo: 2}},
			wantProcessed: 1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "readview 'test' back fill in progress, remaining: 2")
			},
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 2\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &Buffer{}
			logger := slog.New(slog.NewTextHandler(logged, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			sqlDb, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			assert.NoError(t, err)
			mock.MatchExpectationsInOrder(false)
			MigrationExpectations(mock)
			var store eventsourced.EventStore
			if tt.store != nil {
				store = tt.store(t)
			}
			if tt.setup != nil {
				tt.setup(mock)
			}
			db := sqlx.NewDb(sqlDb, "sqlmock")
			var processed int
			var handler = func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
				processed += 1
				return nil
			}
			if tt.handler != nil {
				handler = tt.handler(t)
			}
			resetHandler := ResetHandler(func(ctx context.Context, name string, tx *sqlx.Tx) error {
				return nil
			})
			if tt.resetHandler != nil {
				resetHandler = tt.resetHandler(t)
			}
			rv, err := New(
				db,
				store,
				WithLogger(logger),
				func(r *Readview) error {
					r.sleep = func(d time.Duration) {
						logger.Debug("sleeping", "duration", d)
						time.Sleep(10 * time.Millisecond)
					}
					r.now = func() time.Time {
						return now
					}
					r.hostname = "host"
					return nil
				},
				Register("test", handler, resetHandler),
			)
			assert.NoError(t, err)
			if !tt.processingDisabled {
				enableEventProcessing(rv)
			}

			for _, msg := range tt.messages {
				_, err = rv.Handlers()["test"](msg)
			}
			if !tt.wantErr(t, err, fmt.Sprintf("ReadView_MessageHandler(%v, %v)", db, store)) {
				return
			}
			time.Sleep(400 * time.Millisecond)
			assert.NoError(t, mock.ExpectationsWereMet())

			var gotLogged []string
			if logged.String() != "" {
				gotLogged = strings.Split(logged.String(), "\n")
				gotLogged = gotLogged[:len(gotLogged)-1]
			}

			assert.Equal(t, tt.wantLogged, gotLogged)
		})
	}
}

func TestReadview_Start(t *testing.T) {
	tests := []struct {
		name         string
		store        func(t *testing.T) eventsourced.EventStore
		resetHandler func(t *testing.T) ResetHandler
		setup        func(mock sqlmock.Sqlmock)
		wantErr      assert.ErrorAssertionFunc
		wantLogged   []string
	}{
		{
			name: "store error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, fmt.Errorf("max global seq error")
					},
				}
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "max global seq error")
			},
			wantLogged: nil,
		},
		{
			name: "insert initial value error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnError(fmt.Errorf("insert error"))
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "insert error")
			},
			wantLogged: nil,
		},
		{
			name: "begin transaction error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.
					ExpectBegin().WillReturnError(fmt.Errorf("transaction error"))
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "transaction error")
			},
			wantLogged: nil,
		},
		{
			name: "status query error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnError(fmt.Errorf("query error"))
				mock.ExpectRollback()
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "query error")
			},
			wantLogged: nil,
		},
		{
			name: "status query+rollback error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnError(fmt.Errorf("query error"))
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "query error")
			},
			wantLogged: []string{"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test"},
		},
		{
			name: "backfill not needed",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.ExpectRollback()
			},
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name: "reset requested - reset error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, nil
					},
				}
			},
			resetHandler: func(t *testing.T) ResetHandler {
				return func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return fmt.Errorf("reset error")
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.ExpectRollback()
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "reset readview 'test': reset error")
			},
			wantLogged: nil,
		},
		{
			name: "reset requested - reset success",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, nil
					},
				}
			},
			resetHandler: func(t *testing.T) ResetHandler {
				return func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return nil
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(0, nil, nil),
					)
				mock.ExpectRollback()
			},
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name: "wait for running back fill",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 2, nil
					},
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						c <- TestEvent{
							seqNo: 2,
						}
						c <- nil
						return nil
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(1, "host", time.Time{}),
					)
				mock.ExpectRollback()
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"current_seq_no", "filler", "updated"}).
							AddRow(1, nil, time.Time{}),
					)
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated, filler) VALUES ($1, 0, $3, $2) ON CONFLICT (name) DO UPDATE SET filler = $2, started = $3, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", "host", time.UnixMilli(1000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(3000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(5000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			wantLogged: []string{
				"level=INFO msg=\"waiting for back fill\" readview=test",
				"level=INFO msg=\"starting back fill, events to process: 1\" readview=test",
				"level=INFO msg=\"remaining back fill events: 0/1, estimated time to completion: 0s\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
			wantErr: assert.NoError,
		},
		{
			name: "backfill update status error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 1, nil
					},
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						return fmt.Errorf("events error")
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated, filler) VALUES ($1, 0, $3, $2) ON CONFLICT (name) DO UPDATE SET filler = $2, started = $3, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", "host", time.UnixMilli(1000)).
					WillReturnError(fmt.Errorf("update status error"))
				mock.ExpectRollback()
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
		},
		{
			name: "backfill commit error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 1, nil
					},
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						return fmt.Errorf("events error")
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated, filler) VALUES ($1, 0, $3, $2) ON CONFLICT (name) DO UPDATE SET filler = $2, started = $3, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", "host", time.UnixMilli(1000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit().WillReturnError(fmt.Errorf("commit error"))
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "commit error")
			},
		},
		{
			name: "backfill error",
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 1, nil
					},
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						close(c)
						time.Sleep(100 * time.Millisecond)
						return fmt.Errorf("events error")
					},
				}
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no) VALUES ($1, 0) ON CONFLICT (name) DO NOTHING").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 0))
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated, filler) VALUES ($1, 0, $3, $2) ON CONFLICT (name) DO UPDATE SET filler = $2, started = $3, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", "host", time.UnixMilli(1000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 0, time.UnixMilli(3000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "events error")
			},
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 1\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &Buffer{}
			logger := slog.New(slog.NewTextHandler(logged, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			sqlDb, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			assert.NoError(t, err)
			var store eventsourced.EventStore
			if tt.store != nil {
				store = tt.store(t)
			}
			var resetHandler ResetHandler = func(ctx context.Context, name string, tx *sqlx.Tx) error {
				return nil
			}
			if tt.resetHandler != nil {
				resetHandler = tt.resetHandler(t)
			}
			if tt.setup != nil {
				tt.setup(mock)
			}
			db := sqlx.NewDb(sqlDb, "sqlmock")
			var now int64
			r := &Readview{
				db:                db,
				store:             store,
				viewInfo:          make(map[string]ViewInfo),
				logger:            logger,
				backFillBatchSize: 2,
				hostname:          "host",
				now: func() time.Time {
					now++
					return time.UnixMilli(now * 1000)
				},
				sleep: func(d time.Duration) {
					logger.Debug("sleeping", "duration", d)
					time.Sleep(10 * time.Millisecond)
				},
			}
			err = Register(
				"test",
				func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
				resetHandler,
			)(r)
			assert.NoError(t, err)
			r.init()
			ctx := context.Background()
			tt.wantErr(t, r.Start(ctx), fmt.Sprintf("Start(%v)", ctx))
			time.Sleep(300 * time.Millisecond)
			assert.NoError(t, mock.ExpectationsWereMet())

			var gotLogged []string
			if logged.String() != "" {
				gotLogged = strings.Split(logged.String(), "\n")
				gotLogged = gotLogged[:len(gotLogged)-1]
			}

			assert.Equal(t, tt.wantLogged, gotLogged)
		})
	}
}

func TestReadview_Reset(t *testing.T) {
	now := time.Date(2023, 11, 3, 16, 12, 0, 0, time.Local)
	type args struct {
		name    string
		handler ResetHandler
	}
	tests := []struct {
		name       string
		store      func(t *testing.T) eventsourced.EventStore
		setup      func(mock sqlmock.Sqlmock)
		filling    bool
		args       args
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "begin transaction error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin().WillReturnError(fmt.Errorf("transaction error"))
			},
			args: args{
				name: "test",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "transaction error")
			},
		},
		{
			name: "query error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnError(fmt.Errorf("query error"))
				mock.ExpectRollback()
			},
			args: args{
				name: "test",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "query error")
			},
		},
		{
			name: "query+rollback error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnError(fmt.Errorf("query error"))
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			args: args{
				name: "test",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "query error")
			},
			wantLogged: []string{"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test"},
		},
		{
			name: "unknown readview",
			args: args{
				name: "unknown",
			},
			wantErr:    assert.NoError,
			wantLogged: []string{"level=ERROR msg=\"no readview named 'unknown' exist\""},
		},
		{
			name: "already filling",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}).AddRow("0", "host", now))
				mock.ExpectRollback()
			},
			filling: true,
			args: args{
				name: "test",
			},
			wantErr:    assert.NoError,
			wantLogged: []string{"level=WARN msg=\"backfill already in progress\" readview=test"},
		},
		{
			name: "stale backfill",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}).AddRow(1, "other-host", time.Time{}))
				mock.ExpectRollback()
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return fmt.Errorf("reset handler error")
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "reset handler error")
			},
			wantLogged: nil,
		},
		{
			name: "handler error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.ExpectRollback()
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return fmt.Errorf("reset handler error")
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "reset handler error")
			},
			wantLogged: nil,
		},
		{
			name: "handler error - rollback error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return fmt.Errorf("reset handler error")
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "reset handler error")
			},
			wantLogged: []string{"level=ERROR msg=\"transaction rollback\" error=\"rollback error\" readview=test"},
		},
		{
			name: "update status error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("DELETE FROM readview_status WHERE name = $1").
					WithArgs("test").
					WillReturnError(fmt.Errorf("update status error"))
				mock.ExpectRollback()
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
			wantLogged: nil,
		},
		{
			name: "commit error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("DELETE FROM readview_status WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit().WillReturnError(fmt.Errorf("commit tx error"))
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "commit tx error")
			},
			wantLogged: nil,
		},
		{
			name: "max global seq error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("DELETE FROM readview_status WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
			},
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 0, fmt.Errorf("max global seq error")
					},
				}
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "max global seq error")
			},
			wantLogged: nil,
		},
		{
			name: "events error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("DELETE FROM readview_status WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 0, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
			},
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 2, nil
					},
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						close(c)
						time.Sleep(100 * time.Millisecond)
						return fmt.Errorf("events error")
					},
				}
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "events error")
			},
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 2\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
		{
			name: "success",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectBegin()
				mock.
					ExpectQuery("SELECT current_seq_no, filler, updated FROM readview_status WHERE name = $1 FOR UPDATE NOWAIT").
					WithArgs("test").
					WillReturnRows(sqlmock.NewRows([]string{"current_seq_no", "filler", "updated"}))
				mock.
					ExpectExec("DELETE FROM readview_status WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 0, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
			},
			store: func(t *testing.T) eventsourced.EventStore {
				return &mocks.Store{
					MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
						return 2, nil
					},
					EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
						close(c)
						return nil
					},
				}
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return nil
				},
			},
			wantErr: assert.NoError,
			wantLogged: []string{
				"level=INFO msg=\"starting back fill, events to process: 2\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &Buffer{}
			logger := slog.New(slog.NewTextHandler(logged, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			sqlDb, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			assert.NoError(t, err)
			mock.MatchExpectationsInOrder(false)
			var store eventsourced.EventStore
			if tt.store != nil {
				store = tt.store(t)
			}
			if tt.setup != nil {
				tt.setup(mock)
			}
			db := sqlx.NewDb(sqlDb, "sqlmock")
			var now int64
			r := &Readview{
				db:                db,
				store:             store,
				viewInfo:          make(map[string]ViewInfo),
				logger:            logger,
				backFillBatchSize: 2,
				now: func() time.Time {
					now++
					return time.UnixMilli(now * 1000)
				},
				sleep: func(d time.Duration) {
					logger.Debug("sleeping", "duration", d)
					time.Sleep(10 * time.Millisecond)
				},
			}
			err = Register(
				"test",
				func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
				tt.args.handler,
			)(r)
			assert.NoError(t, err)
			//enableEventProcessing(r)
			r.init()
			ctx := context.Background()
			tt.wantErr(t, r.Reset(ctx, tt.args.name), fmt.Sprintf("Reset(%v, %v, %v)", ctx, tt.args.name, tt.args.handler))
			time.Sleep(200 * time.Millisecond)
			assert.NoError(t, mock.ExpectationsWereMet())

			var gotLogged []string
			if logged.String() != "" {
				gotLogged = strings.Split(logged.String(), "\n")
				gotLogged = gotLogged[:len(gotLogged)-1]
			}

			assert.Equal(t, tt.wantLogged, gotLogged)
		})
	}
}

func TestReadview_doBackFill(t *testing.T) {
	type args struct {
		name       string
		handler    EventHandler
		sequenceNo int
	}
	tests := []struct {
		name       string
		store      func(t *testing.T) eventsourced.EventStore
		setup      func(mock sqlmock.Sqlmock)
		messages   []eventsourced.Event
		args       args
		cancel     bool
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name:  "begin tx error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin().
					WillReturnError(fmt.Errorf("begin tx error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: nil,
			args: args{
				name: "test",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "begin tx error")
			},
			wantLogged: []string{"level=ERROR msg=\"create tx\" error=\"begin tx error\" readview=test"},
		},
		{
			name:  "cancel context",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.ExpectRollback()
			},
			messages: nil,
			args: args{
				name: "test",
			},
			cancel:  true,
			wantErr: assert.NoError,
			wantLogged: []string{
				"level=INFO msg=\"root context done\"",
				"level=ERROR msg=\"reset back fill status\" error=\"context canceled\" readview=test",
			},
		},
		{
			name:  "handler error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectRollback().
					WillReturnError(fmt.Errorf("rollback error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 1}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return fmt.Errorf("handler error")
				},
			},
			cancel: true,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "handler error")
			},
			wantLogged: []string{
				"level=ERROR msg=\"process back fill event 1\" error=\"handler error\" readview=test",
				"level=ERROR msg=\"rollback tx\" error=\"rollback error\" readview=test",
			},
		},
		{
			name:  "all messages processed - update status error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, time.UnixMilli(2000)).
					WillReturnError(fmt.Errorf("update status error"))
				mock.
					ExpectRollback().
					WillReturnError(fmt.Errorf("rollback error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 1}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
			wantLogged: []string{
				"level=INFO msg=\"events processed\" readview=test",
				"level=ERROR msg=\"update current seq_no, rolling back\" error=\"update status error\" readview=test",
				"level=ERROR msg=\"rollback tx\" error=\"rollback error\" readview=test",
			},
		},
		{
			name:  "all messages processed - tx commit error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit().WillReturnError(fmt.Errorf("commit tx error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 1}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "commit tx error")
			},
			wantLogged: []string{
				"level=INFO msg=\"events processed\" readview=test",
				"level=ERROR msg=\"commit tx\" error=\"commit tx error\" readview=test",
			},
		},
		{
			name:  "all messages processed - success",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 1}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
			},
			wantErr: assert.NoError,
			wantLogged: []string{
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
		{
			name:  "all messages processed - success - external event",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 1, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{
				&eventsourced.ExternalEvent[*ExternalEvent]{
					BaseEvent: eventsourced.BaseEvent{
						EventGlobalSequence: eventsourced.EventGlobalSequence{SeqNo: 1},
					},
					WrappedEvent: &ExternalEvent{},
				},
			},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					assert.IsType(t, &ExternalEvent{}, event)
					return nil
				},
			},
			wantErr: assert.NoError,
			wantLogged: []string{
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
		{
			name:  "all messages processed - full batch - update status error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(2000)).
					WillReturnError(fmt.Errorf("update status error"))
				mock.ExpectRollback()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 2}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
			wantLogged: []string{"level=ERROR msg=\"update current seq_no, rolling back\" error=\"update status error\" readview=test"},
		},
		{
			name:  "all messages processed - full batch - rollback error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(2000)).
					WillReturnError(fmt.Errorf("update status error"))
				mock.ExpectRollback().WillReturnError(fmt.Errorf("rollback error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 2}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update status error")
			},
			wantLogged: []string{
				"level=ERROR msg=\"update current seq_no, rolling back\" error=\"update status error\" readview=test",
				"level=ERROR msg=\"rollback tx\" error=\"rollback error\" readview=test",
			},
		},
		{
			name:  "all messages processed - full batch - commit error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit().WillReturnError(fmt.Errorf("commit error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 2}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "commit error")
			},
			wantLogged: []string{"level=ERROR msg=\"commit tx\" error=\"commit error\" readview=test"},
		},
		{
			name:  "all messages processed - full batch - new tx error",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin().WillReturnError(fmt.Errorf("tx error"))
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 2}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "tx error")
			},
			wantLogged: []string{"level=ERROR msg=\"create tx\" error=\"tx error\" readview=test"},
		},
		{
			name:  "all messages processed - full batch - success",
			store: nil,
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(2000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectBegin()
				mock.
					ExpectExec("INSERT INTO readview_status (name, current_seq_no, updated) VALUES ($1, $2, $3) ON CONFLICT (name) DO UPDATE SET current_seq_no = $2, updated = $3 WHERE readview_status.name = $1").
					WithArgs("test", 2, time.UnixMilli(4000)).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.
					ExpectExec("UPDATE readview_status SET filler = '', started = NULL WHERE name = $1").
					WithArgs("test").
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			messages: []eventsourced.Event{&TestEvent{seqNo: 2}},
			args: args{
				name: "test",
				handler: func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
				sequenceNo: 2,
			},
			wantErr: assert.NoError,
			wantLogged: []string{
				"level=INFO msg=\"remaining back fill events: 0/2, estimated time to completion: 0s\" readview=test",
				"level=INFO msg=\"events processed\" readview=test",
				"level=INFO msg=\"back fill complete\" readview=test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &Buffer{}
			logger := slog.New(slog.NewTextHandler(logged, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			sqlDb, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			assert.NoError(t, err)
			var store eventsourced.EventStore
			if tt.store != nil {
				store = tt.store(t)
			}
			if tt.setup != nil {
				tt.setup(mock)
			}
			db := sqlx.NewDb(sqlDb, "sqlmock")
			var now int64
			r := &Readview{
				db:                db,
				store:             store,
				viewInfo:          make(map[string]ViewInfo),
				logger:            logger,
				backFillBatchSize: 2,
				now: func() time.Time {
					now++
					return time.UnixMilli(now * 1000)
				},
				sleep: func(d time.Duration) {
					logger.Debug("sleeping", "duration", d)
					time.Sleep(10 * time.Millisecond)
				},
			}
			err = Register(
				tt.args.name,
				func(ctx context.Context, tx *sql.Tx, event eventsourced.Event) error {
					return nil
				},
				func(ctx context.Context, name string, tx *sqlx.Tx) error {
					return nil
				},
			)(r)
			assert.NoError(t, err)
			r.init()
			ctx, cancel := context.WithCancel(context.Background())
			if tt.cancel {
				go func() {
					time.Sleep(300 * time.Millisecond)
					cancel()
				}()
			}
			ch := make(chan eventsourced.Event, 10)
			for _, msg := range tt.messages {
				ch <- msg
			}
			if !tt.cancel {
				go func() {
					time.Sleep(400 * time.Millisecond)
					close(ch)
				}()
			}
			tt.wantErr(t, r.doBackFill(ctx, tt.args.name, tt.args.handler, ch, tt.args.sequenceNo, 0), fmt.Sprintf("doBackFill(%v, %v, %v, %v, %v)", ctx, tt.args.name, tt.args.handler, ch, tt.args.sequenceNo))
			assert.NoError(t, mock.ExpectationsWereMet())

			cancel()
			var gotLogged []string
			if logged.String() != "" {
				gotLogged = strings.Split(logged.String(), "\n")
				gotLogged = gotLogged[:len(gotLogged)-1]
			}

			assert.Equal(t, tt.wantLogged, gotLogged)
		})
	}
}

func TestWithBackFillBatchSize(t *testing.T) {
	r := &Readview{}
	err := WithBackFillBatchSize(100)(r)
	assert.NoError(t, err)
	assert.Equal(t, 100, r.backFillBatchSize)
}

func TestWithHeartbeat(t *testing.T) {
	r := &Readview{}
	err := WithHeartbeat(time.Second * 3)(r)
	assert.NoError(t, err)
	assert.Equal(t, time.Second*3, r.heartbeat)
}

func Test_gooseLoggerWrapper_Fatalf(t *testing.T) {
	type args struct {
		format string
		v      []interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "no vars",
			args: args{
				format: "abc",
			},
			want: "level=ERROR msg=abc part=readview\n",
		},
		{
			name: "with vars",
			args: args{
				format: "abc %s",
				v:      []interface{}{"xxx"},
			},
			want: "level=ERROR msg=\"abc xxx\" part=readview\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buff := &bytes.Buffer{}
			logger := slog.New(slog.NewTextHandler(buff, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			g := gooseLoggerWrapper{
				logger: logger,
			}
			g.Fatalf(tt.args.format, tt.args.v...)
			assert.Equal(t, tt.want, buff.String())
		})
	}
}

func Test_gooseLoggerWrapper_Printf(t *testing.T) {
	type args struct {
		format string
		v      []interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "no vars",
			args: args{
				format: "abc",
			},
			want: "level=INFO msg=abc part=readview\n",
		},
		{
			name: "with vars",
			args: args{
				format: "abc %s",
				v:      []interface{}{"xxx"},
			},
			want: "level=INFO msg=\"abc xxx\" part=readview\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buff := &bytes.Buffer{}
			logger := slog.New(slog.NewTextHandler(buff, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			g := gooseLoggerWrapper{
				logger: logger,
			}
			g.Printf(tt.args.format, tt.args.v...)
			assert.Equal(t, tt.want, buff.String())
		})
	}
}

func Test_calcRemainingTime(t *testing.T) {
	type args struct {
		remaining   int
		total       int
		processTime time.Duration
	}
	tests := []struct {
		name string
		args args
		want time.Duration
	}{
		{
			name: "1 second",
			args: args{
				remaining:   100,
				total:       6000,
				processTime: 59 * time.Second,
			},
			want: time.Second,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calcRemainingTime(tt.args.remaining, tt.args.total, tt.args.processTime); got != tt.want {
				t.Errorf("calcRemainingTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReadview_backFillInProgress(t *testing.T) {
	type fields struct {
		hostname  string
		heartbeat time.Duration
		now       func() time.Time
	}
	type args struct {
		filler  *string
		updated *time.Time
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "no filler",
			fields: fields{},
			args:   args{},
			want:   false,
		},
		{
			name:   "blank filler",
			fields: fields{},
			args: args{
				filler: ptr(""),
			},
			want: false,
		},
		{
			name: "hostname same as filler",
			fields: fields{
				hostname: "host",
			},
			args: args{
				filler: ptr("host"),
			},
			want: true,
		},
		{
			name: "updated is nil",
			fields: fields{
				hostname: "host",
			},
			args: args{
				filler: ptr("other"),
			},
			want: false,
		},
		{
			name: "updated within heartbeat",
			fields: fields{
				hostname: "host",
				now: func() time.Time {
					return time.Date(2024, 1, 1, 0, 0, 0, 0, time.UTC)
				},
				heartbeat: time.Minute,
			},
			args: args{
				filler:  ptr("other"),
				updated: ptr(time.Date(2024, 1, 1, 0, 0, 10, 0, time.UTC)),
			},
			want: true,
		},
		{
			name: "updated older than heartbeat",
			fields: fields{
				hostname: "host",
				now: func() time.Time {
					return time.Date(2024, 1, 1, 0, 2, 0, 0, time.UTC)
				},
				heartbeat: time.Minute,
			},
			args: args{
				filler:  ptr("other"),
				updated: ptr(time.Date(2024, 1, 1, 0, 0, 10, 0, time.UTC)),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Readview{
				hostname:  tt.fields.hostname,
				heartbeat: tt.fields.heartbeat,
				now:       tt.fields.now,
			}
			assert.Equalf(t, tt.want, r.backFillInProgress(tt.args.filler, tt.args.updated), "backFillInProgress(%v, %v)", tt.args.filler, tt.args.updated)
		})
	}
}

func MigrationExpectations(mock sqlmock.Sqlmock) {
	mock.ExpectQuery("SELECT EXISTS ( SELECT 1 FROM pg_tables WHERE (current_schema() IS NULL OR schemaname = current_schema()) AND tablename = 'goose_db_version_readview' )").
		WithArgs().WillReturnRows(sqlmock.NewRows([]string{""}).AddRow("true"))
	mock.ExpectQuery("SELECT version_id, is_applied from goose_db_version_readview ORDER BY id DESC").
		WillReturnRows(sqlmock.
			NewRows([]string{"tstamp", "is_applied"}).
			AddRow(20230928170600, true).
			AddRow(20231030090500, true).
			AddRow(20231103095600, true).
			AddRow(20240320132000, true),
		)
}

type TestEvent struct {
	seqNo int
}

func (e TestEvent) AggregateIdentity() eventsourced.ID {
	panic("implement me")
}

func (e TestEvent) SetAggregateIdentity(eventsourced.ID) {
	panic("implement me")
}

func (e TestEvent) When() time.Time {
	panic("implement me")
}

func (e TestEvent) SetWhen(time.Time) {
	panic("implement me")
}

func (e TestEvent) GlobalSequenceNo() int {
	return e.seqNo
}

func (e TestEvent) SetGlobalSequenceNo(int) {
	panic("implement me")
}

var _ eventsourced.Event = &TestEvent{}

type ExternalEvent struct {
	seqNo int
}

func (e ExternalEvent) ExternalSource() string {
	return "external"
}

func (e ExternalEvent) AggregateIdentity() eventsourced.ID {
	panic("implement me")
}

func (e ExternalEvent) SetAggregateIdentity(eventsourced.ID) {
	panic("implement me")
}

func (e ExternalEvent) When() time.Time {
	panic("implement me")
}

func (e ExternalEvent) SetWhen(t time.Time) {
	panic("implement me")
}

func (e ExternalEvent) GlobalSequenceNo() int {
	return e.seqNo
}

func (e ExternalEvent) SetGlobalSequenceNo(seqNo int) {
	panic("implement me")
}

var _ eventsourced.Event = &ExternalEvent{}

var _ eventsourced.ExternalSource = &ExternalEvent{}

type Buffer struct {
	b bytes.Buffer
	m sync.Mutex
}

func (b *Buffer) Read(p []byte) (n int, err error) {
	b.m.Lock()
	defer b.m.Unlock()
	return b.b.Read(p)
}

func (b *Buffer) Write(p []byte) (n int, err error) {
	b.m.Lock()
	defer b.m.Unlock()
	return b.b.Write(p)
}

func (b *Buffer) String() string {
	b.m.Lock()
	defer b.m.Unlock()
	return b.b.String()
}

func ptr[T any](v T) *T {
	return &v
}

func enableEventProcessing(r *Readview) {
	for _, v := range r.viewInfo {
		v.readyForEvents.Store(true)
	}
}
